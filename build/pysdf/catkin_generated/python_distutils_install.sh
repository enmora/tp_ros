#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/local.isima.fr/enmora/Downloads/tp3_workspace/src/pysdf"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/local.isima.fr/enmora/Downloads/tp3_workspace/install/lib/python3/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/local.isima.fr/enmora/Downloads/tp3_workspace/install/lib/python3/dist-packages:/home/local.isima.fr/enmora/Downloads/tp3_workspace/build/lib/python3/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/local.isima.fr/enmora/Downloads/tp3_workspace/build" \
    "/usr/bin/python3" \
    "/home/local.isima.fr/enmora/Downloads/tp3_workspace/src/pysdf/setup.py" \
     \
    build --build-base "/home/local.isima.fr/enmora/Downloads/tp3_workspace/build/pysdf" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/local.isima.fr/enmora/Downloads/tp3_workspace/install" --install-scripts="/home/local.isima.fr/enmora/Downloads/tp3_workspace/install/bin"
